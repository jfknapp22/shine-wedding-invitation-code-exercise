import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    const keys = [...Array(8).keys()];
    const prevOrder = JSON.parse(localStorage.getItem("order"));
    const order = prevOrder && prevOrder.length > 0 ? prevOrder : keys;

    const clicks = new Map(
      keys.map(key => {
        return [key, 0];
      })
    );

    this.state = {
      order: order,
      clicks: clicks,
      orderBuffer: [],
      isRefreshButtonVisible: false
    };
  }

  onImageClick = e => {
    const clicked = Number.parseInt(e.target.dataset.key);

    const newValue = this.state.clicks.get(clicked) + 1;
    const map = this.state.clicks;
    map.set(clicked, newValue);
    const sortedMap = new Map(Array.from(map).sort((a, b) => { return b[1] - a[1]; }));

    if (this.state.orderBuffer.indexOf(clicked) === -1) {
      const newOrderBuffer = this.state.orderBuffer;
      newOrderBuffer.push(clicked);
      
      this.setState({
        orderBuffer: newOrderBuffer
      });
    }

    if (this.state.orderBuffer.length === this.state.order.length) {
      localStorage.setItem("order", JSON.stringify(this.state.orderBuffer));
      this.setState({
        isRefreshButtonVisible: true
      });
    }

    this.setState({
      clicks: sortedMap
    });
  };

  render() {
    let refreshButton = "";

    if (this.state.isRefreshButtonVisible) {
      refreshButton = (
        <div>
          <button onClick={e => { window.location.reload() }}>Click to see changed order</button>
        </div>
      );
    }

    return (
      <div className="App">
        <div className="grid-container">
          {this.state.order.map(key => {
            return (
              <div className="grid-item" key={key}>
                <img
                  src={
                    "https://via.placeholder.com/100?text=image" +
                    key.toString()
                  }
                  data-key={key}
                  onClick={this.onImageClick}
                />
              </div>
            );
          })}
        </div>
        {refreshButton}

        <div>
          <h4>Click counts:</h4>

          <ol>
            {Array.from(this.state.clicks.keys()).map(key => {
              return (
                <li key={key.toString()}>
                  {"Image #" + key.toString()} - {this.state.clicks.get(key)}
                </li>
              );
            })}
          </ol>
        </div>
      </div>
    );
  }
}

export default App;
