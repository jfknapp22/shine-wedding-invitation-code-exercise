
# Shine Coding Exercise

Using html, css, and javascript (any javascript libraries such as React, JQuery, etc can be used if desired), take the following list of eight images and lay them out on a single web page in a 2x4 grid (grid is 2 units wide):

    https://via.placeholder.com/100?text=image0
    https://via.placeholder.com/100?text=image1
    https://via.placeholder.com/100?text=image2
    https://via.placeholder.com/100?text=image3
    https://via.placeholder.com/100?text=image4
    https://via.placeholder.com/100?text=image5
    https://via.placeholder.com/100?text=image6
    https://via.placeholder.com/100?text=image7


On the same web page, perform these two different tasks with image clicks:

    Handle click events on the <img> elements so you can keep track of the order in which the items were clicked.  Each <img> element should only allow a single click to specify its order. Save the order of the images to local storage and then read the order of the images back from local storage on page load. Use the stored order on page load (if set) to set the initial order of the images; otherwise use the previous ordering. All images should be shown on page load/load regardless of how many images were clicked previously.

    Count image clicks on each image and sort the click counts so that the images with the most click counts are listed in descending order. Display or log this information

When you are done, identify the parts of this task were familiar/easy, and which were new or unfamiliar. Briefly discuss any coding decisions you made that you feel need further explanation.